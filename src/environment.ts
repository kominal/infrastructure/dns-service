export const DOMAINS = process.env.DOMAINS || '';
export const ENTRYPOINTS = process.env.ENTRYPOINTS || '';
export const NAMESERVER = process.env.NAMESERVER || '';
export const TXT_RECORDS = process.env.TXT_RECORDS || '';
