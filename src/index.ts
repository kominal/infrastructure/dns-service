import Service from '@kominal/service-util/helper/service';
import { startDNSServer } from './dns-server';
import { checkEntrypointsHealth } from './scheduler';
import { ENTRYPOINTS } from './environment';
import { error } from '@kominal/lib-ts-logging';

export const service = new Service({
	id: 'dns-service',
	name: 'DNS Service',
	description: 'Answers DNS requests using predefined entrypoint IP addresses and checks health of those periodically.',
	jsonLimit: '16mb',
	routes: [],
	scheduler: [{ task: checkEntrypointsHealth, squad: false, frequency: 30 }],
});

async function start() {
	if (ENTRYPOINTS.split(',').length === 0) {
		error('No entrypoints available.');
		process.exit(0);
	}

	await service.start();
	startDNSServer();
}

start();
