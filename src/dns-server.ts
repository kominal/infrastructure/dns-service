import { debug, error } from '@kominal/lib-ts-logging';
import { isIPv6 } from 'net';
import { DOMAINS, NAMESERVER, TXT_RECORDS } from './environment';
import { generateNSRecords, generateSOARecords } from './helper';
import { entrypoints } from './scheduler';

const dnsd: any = require('dnsd');

const handler = (req: any, res: any) => {
	try {
		const { name, type } = res.question[0];

		res.authoritative = true;

		debug(`Answering request for ${name} - ${type} from ${req.connection.remoteAddress}...`);

		if (type === 'SOA') {
			generateSOARecords(name).forEach((record) => res.answer.push(record));
		} else if (type === 'NS') {
			generateNSRecords(name).forEach((record) => res.answer.push(record));
		} else if (type === 'TXT') {
			for (const data of TXT_RECORDS.split(',')) {
				res.answer.push({ name, ttl: 3600, class: 'IN', type, data });
			}
		} else if (entrypoints.length > 0) {
			for (const entrypoint of entrypoints.sort(() => 0.5 - Math.random())) {
				res.answer.push({ name, ttl: 3600, class: 'IN', type: isIPv6(entrypoint) ? 'AAAA' : 'A', data: entrypoint });
			}
		} else {
			error('No working host found.');
			generateSOARecords(name).forEach((record) => res.authority.push(record));
		}

		res.end();
	} catch (e) {
		console.log(e);
	}
};

export function startDNSServer() {
	let server = dnsd.createServer(handler);

	const primaryNameserver = NAMESERVER.split(',')[0];

	for (const domain of DOMAINS.split(',')) {
		server = server.zone(domain, primaryNameserver, 'mail@kominal.com', 'now', '2h', '30m', '2w', '10m');
	}

	server.listen(53, '0.0.0.0');
}
