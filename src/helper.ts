import { NAMESERVER } from './environment';

export function generateSOARecords(name: string): any[] {
	return NAMESERVER.split(',').map((mname) => {
		return {
			class: 'IN',
			type: 'SOA',
			name,
			data: {
				mname,
				rname: 'mail@kominal.com',
				serial: getSerial(),
				refresh: 7200,
				retry: 1800,
				expire: 1209600,
				ttl: 600,
			},
		};
	});
}

export function generateNSRecords(name: string): any[] {
	return NAMESERVER.split(',').map((data) => {
		return {
			class: 'IN',
			type: 'NS',
			name,
			data,
		};
	});
}

function getSerial() {
	var d = new Date(),
		month = '' + (d.getMonth() + 1),
		seconds = '' + d.getSeconds(),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;
	if (seconds.length < 2) seconds = '0' + seconds;

	return [year, month, day, seconds].join('');
}
