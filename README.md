# DNS Service

Kominal uses two steps to load balance traffic.
The DNS service is used for the first step.
Whenever a client wants to interact with a connect service it has to request a suitable edge node from the DNS service.
The DNS service checks the health of each edge node and only replies with a working nodes.

The second load balance step is used to load balance the traffic in the internal network.
While the DNS service only balances the traffic between the external network and the internal edge nodes, the second step load balances the traffic to each of the running pods of a service. Those pods don't have to be K8s pods but can also be plain old docker container. The second step checks the health of each pod and only redirects traffic to working pods.
